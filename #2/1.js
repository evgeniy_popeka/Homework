
  /*

    Задание 1.

    Написать скрипт который будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
    selButton = document.getElementById ("buttonContainer");
    selButton.onclick = function(event) {
        var button = document.getElementsByClassName('tab');
        var x = event.target.dataset.tab;
        // console.log(x);

        var tabs = document.querySelectorAll(".tab");
        console.log(tabs);
        tabs.forEach( function(item){
        console.log(item);
            if( item.dataset.tab === x){
                item.classList.add('active');
            } else {
                item.classList.remove('active');
            };
        });

};
