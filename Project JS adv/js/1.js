let imageCollect = ["image/1.jpeg", "image/2.jpeg", "image/3.jpeg", "image/4.jpeg", "image/5.jpeg",
                    "image/6.jpeg", "image/7.jpeg", "image/8.jpeg", "image/9.jpeg", "image/10.jpeg",
                    "image/11.jpeg", "image/12.jpeg", "image/13.jpeg", "image/14.jpeg", "image/15.jpeg",
                    "image/16.jpeg", "image/17.jpeg", "image/18.jpeg", "image/19.jpeg", "image/20.jpeg",];
newElement();

function newElement (){

    imageCollect.forEach(function(item){

        let divSlide = document.createElement("div");
        divSlide.classList = "slide";
        divSlide.style.backgroundImage ="url("+item+")";

        let divContent = document.createElement("div");
        divContent.classList = "slide-content";
        divSlide.appendChild(divContent);
        document.getElementById("slider").appendChild(divSlide);
    });
}

let sliderImages = document.querySelectorAll(".slide"),
  arrowLeft = document.querySelector("#arrow-left"),
  arrowRight = document.querySelector("#arrow-right"),
  current = 0;

function reset() {
  for (let i = 0; i < sliderImages.length; i++) {
    sliderImages[i].style.display = "none";
  }
}

function startSlide() {
  reset();
  sliderImages[0].style.display = "block";
}

function slideLeft() {
  reset();
  sliderImages[current - 1].style.display = "block";
  current--;
}

function slideRight() {
  reset();
  sliderImages[current + 1].style.display = "block";
  current++;
}

arrowLeft.addEventListener("click", function() {
  if (current == 0) {
    current = sliderImages.length;
  }
  slideLeft();
});

arrowRight.addEventListener("click", function() {
  if (current == sliderImages.length - 1) {
    current = -1;
  }
  slideRight();
});

startSlide();
